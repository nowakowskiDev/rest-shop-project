package pl.nowakowski.dev.restshopproject.restshopproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestShopProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestShopProjectApplication.class, args);
	}

}
